import * as E from "fp-ts/lib/Either"
import { pipe } from "fp-ts/lib/function"

type MyErr = string
type PositiveDecimal = number

const divideTwo = (dec: PositiveDecimal): E.Either<MyErr, PositiveDecimal> =>
  pipe(
    dec < 0 ? E.left("cannot be negative") : E.right(dec),
    E.chain((d) => (d > 1 ? E.left(">=1") : E.right(dec))),
    E.map((d) => d / 2)
  )

console.log(divideTwo(-1))
// E.left('cannot be negative')

console.log(divideTwo(5))
// E.left('>= 1')

console.log(divideTwo(0.8))
// E.right(0.4)
