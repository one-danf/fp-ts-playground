import * as O from 'fp-ts/lib/Option'
import { add5Subtract1 } from './01-option-map'

describe('option-map', () => {
  it('add5Subtract1 returns an option with the proper calculation', () => {
    expect(add5Subtract1(10)).toStrictEqual(O.some(14))
  })
  it('add5Subtract1 returns an option none if input is undefined', () => {
    expect(add5Subtract1()).toStrictEqual(O.none)
  })
})
