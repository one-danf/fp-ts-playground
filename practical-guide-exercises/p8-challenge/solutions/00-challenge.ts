import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/TaskEither'
import { getAndParseCards } from './02-get-and-parse-cards'
import { validateCards } from './03-validate-cards'
import { formatCards } from './04-format-cards'
import { CardNetwork, CardStatus, GetCards } from '../card-api'
import { CardErrors } from '../errors'
import { DisplayCard } from './01-types'

interface I {
  userId: string
  networkType?: CardNetwork
  cardStatus?: CardStatus
  getCards: GetCards
}

export type GetCardsHandler = ({
  userId,
  networkType,
  cardStatus,
  getCards,
}: I) => TE.TaskEither<CardErrors, DisplayCard[]>

export const getCardsHandler: GetCardsHandler = ({ userId, networkType, cardStatus, getCards }) =>
  pipe(
    getAndParseCards(getCards)(userId),
    TE.chainW(parsedCards =>
      TE.fromEither(validateCards({ networkType, cardStatus })(parsedCards))
    ),
    TE.map(validatedCards => formatCards(validatedCards))
  )
