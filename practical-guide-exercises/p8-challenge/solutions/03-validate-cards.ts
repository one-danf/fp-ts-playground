import { Card } from './01-types'
import { CardNetwork, CardStatus } from '../card-api'
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Array'
import * as NEA from 'fp-ts/NonEmptyArray'
import { NoValidCardsFoundError } from '../errors'
import { pipe } from 'fp-ts/lib/function'

export const validateCards = ({
  networkType,
  cardStatus,
}: {
  networkType?: CardNetwork
  cardStatus?: CardStatus
}) => (cards: Card[]): E.Either<NoValidCardsFoundError, Card[]> =>
  pipe(
    cards,
    A.filter(card => (networkType ? networkType === card.network : true)),
    A.filter(card => (cardStatus ? cardStatus === card.status : true)),
    NEA.fromArray,
    E.fromOption(() => new NoValidCardsFoundError())
  )
