import * as TE from 'fp-ts/TaskEither'

export type CardStatus = 'ACTIVE' | 'INACTIVE'
export type CardNetwork = 'VISA' | 'MASTERCARD'

export interface BaseAddress {
  first_name?: string
  last_name?: string
  address_1?: string
  address_2?: string
  city?: string
  state?: string
  zip?: string
  country?: string
}

export interface BaseCard {
  id?: string
  status?: CardStatus
  network?: CardNetwork
  last4?: string
  pan?: string
  fulfillment?: {
    recipient_address?: BaseAddress
    recipient_mailing_address?: BaseAddress
  }
  card_art?: {
    front_image?: string
    back_image?: string
  }
}

export type GetCards = (userId: string) => TE.TaskEither<Error, BaseCard[]>
