/**
 * Be sure to read part 3 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-3
 *
 * Exercise:
 *  - Modify add5Task to fix the type error by returing a Task<number> instead of number
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 01-task.ts
 *
 * Testing this file.
 *  - npm run test 01-task
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../utils'

// TODO modify add5Task to return a Task<number> instead of number
// and fix the type error
export const add5Task = (number: number): T.Task<number> => number + 5

// No need to modify below here, for running this file
const logAndReturnResult = (number: number) =>
  pipe(
    add5Task(number),
    T.map(result =>
      pipe(
        console.dir(`add5Task(${number}):`),
        () => console.dir(result),
        () => result
      )
    )
  )

pipe(logAndReturnResult(5), T.chain(logAndReturnResult), runIfCli(module))
