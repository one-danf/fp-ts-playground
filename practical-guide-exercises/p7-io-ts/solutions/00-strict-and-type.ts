/**
 * Exercise:
 * - Implement the UserBase interface as User using io-ts strict
 * - Implement the AccountBase interface Account using io-ts type
 */
import * as t from 'io-ts'

interface UserBase {
  user_id: string
  first_name: string
  last_name: string
  email: string
  age: number
  address: {
    address_1: string
    city: string
    state: string
    zip: string
    country: string
  }
}

interface AccountBase {
  account_id: string
  user_id: string
  account_numbers: number[]
  metadata: { [x: string]: string }
  is_verified: boolean
}

// TODO Update below here
export const User = t.strict({
  user_id: t.string,
  first_name: t.string,
  last_name: t.string,
  email: t.string,
  age: t.number,
  address: t.strict({
    address_1: t.string,
    city: t.string,
    state: t.string,
    zip: t.string,
    country: t.string,
  }),
})
export const Account = t.type({
  account_id: t.string,
  user_id: t.string,
  account_numbers: t.array(t.number),
  is_verified: t.boolean,
})

// No need to modify below here
export type User = t.TypeOf<typeof User>
export type Account = t.TypeOf<typeof Account>
