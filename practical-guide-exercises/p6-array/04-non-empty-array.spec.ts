import { validateAnyContainerWithMoreThan5Packages } from './04-non-empty-array'

describe('non-empty-array', () => {
  it.each`
    testNum | containers                                                                                                  | expectedOutput
    ${1}    | ${[]}                                                                                                       | ${false}
    ${2}    | ${[{ id: '1', packages: 10 }]}                                                                              | ${true}
    ${3}    | ${[{ id: '1', packages: 1 }, { id: '2', packages: 2 }, { id: '3', packages: 3 }, { id: '4', packages: 4 }]} | ${false}
    ${4}    | ${[{ id: '1', packages: 5 }, { id: '2', packages: 5 }, { id: '3', packages: 5 }, { id: '4', packages: 5 }]} | ${false}
    ${5}    | ${[{ id: '1', packages: 1 }, { id: '2', packages: 2 }, { id: '3', packages: 3 }, { id: '4', packages: 6 }]} | ${true}
  `(`handles containers for test case $testNum`, ({ containers, expectedOutput }) => {
    expect(validateAnyContainerWithMoreThan5Packages(containers)).toStrictEqual(expectedOutput)
  })
})
