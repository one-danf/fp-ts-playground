import { getTotalWeightOfParcels } from './03-reduce'

describe('reduce', () => {
  it.each`
    testNum | parcels                                                                                               | expectedOutput
    ${1}    | ${[]}                                                                                                 | ${0}
    ${2}    | ${[{ id: '1', weight: 10 }]}                                                                          | ${10}
    ${3}    | ${[{ id: '1', weight: 10 }, { id: '2', weight: 5 }, { id: '3', weight: 8 }, { id: '4', weight: 32 }]} | ${55}
  `(`handles parcels for test case $testNum`, ({ parcels, expectedOutput }) => {
    expect(getTotalWeightOfParcels(parcels)).toStrictEqual(expectedOutput)
  })
})
